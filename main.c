/*
 * main.c
 *
 *  Created on: 11.06.2018
 *      Author: Fabian Spottog
 *      Ein Bespiel welches mit Hilfe des Timer 0 die LED blinken lässt.
 *
 *      Das letzte Beispielprogramm, Timer0Example demonstriert die Funktion des Timer0. Das Programm befindet sich komplett in der main.c und besteht nur aus der main Funktion und einer Interrupt Service Routine. In der main Funktion wird der LED Pin PB2 als Ausgang konfiguriert und der Timer0 und die Interrupts über seine Register entsprechend eingestellt. Anschließend geht der Microcontroller in eine Endlosschleife ohne Funktion.
Wenn ein Interrupt vom Timer0 auftritt wird die LED getoggelt.
Im Unterschied zum Hallo Welt Programm, sieht man bei diesem Programm die LED nicht blinken, sondern nur leuchten. Ursache ist, dass die LED mit 500 Hz blinkt, was das menschliche Auge nicht sehen kann, sondern nur mit einem Oszilloskop zu erkennen ist.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#if ! defined(__AVR_ATtiny85__)
	#error "Wrong AVR Type!"
#endif
int main(void){
    DDRB = 0x00;
    PORTB = 0x00;
//	// Setze die LED Als Ausgang
	DDRB |= 1 << DDB2;
	//Timer 0 Konfigurieren
	TCCR0A = (1<<WGM01);
	TCCR0B |= (1<<CS01);
	OCR0A = 125-1;
	// Compare Interrupt erlauben.
	TIMSK |= (1<<OCIE0A);
	//Globale Interrupt aktivieren.
	sei();
	while(1){
		//nichts tun.
	}
}
////Interrupt beim Erreichen des Timers
ISR(TIMER0_COMPA_vect){
	//Toggle LED
	PORTB ^= (1 << DDB2);
}
